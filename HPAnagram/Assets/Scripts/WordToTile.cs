﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WordToTile : MonoBehaviour
{
    public string wordToPlayWith;
    public GameObject goToPutWordsIn;
    public GameObject prefabToSpawn;
    public GameObject parentToFoster;

    public GameObject emptyPrefabToSpawn;
    public GameObject emptyWordsHere;
    public List<char> letterList = new List<char>();
    public List<char> wordAsList = new List<char>();

    public List<Sprite> alphabet = new List<Sprite>();
    public List<char> alphabetChar = new List<char>();

    public List<GameObject> TopTiles = new List<GameObject>();
    public List<GameObject> BottomTiles = new List<GameObject>();


    public Dictionary<string, Sprite> stringToSprite = new Dictionary<string, Sprite>();
    
    
    // Start is called before the first frame update
    void Start()
    {
        //PopulateDictionary();
        
        
    }

    public void PopulateDictionary()
    {
        for (var i = 0; i < alphabetChar.Count; i++)
        {
            stringToSprite.Add(alphabetChar[i].ToString(),alphabet[i]);
        }
    }

    public void PullLetter (){
        
        for (int i = 0; i < wordToPlayWith.Length; i++)
        {
            letterList.Add(wordToPlayWith[i]);
            wordAsList.Add(wordToPlayWith[i]);
            
        }
        
        
        LetterShuffle();
        
    }

    public void LetterShuffle()
    {
        for (int i = 0; i < letterList.Count; i++)
        {
            char fruitCurrentIndex = letterList[i];
            int randomIndex = Random.Range(i, letterList.Count);
            letterList[i] = letterList[randomIndex];
            letterList[randomIndex] = fruitCurrentIndex;
        }

        string temp = new string(letterList.ToArray());
        if (temp == wordToPlayWith)
        {
            LetterShuffle();
        }
        else
        {
            SpawnLetter();
        }
    }

    public void SpawnLetter (){
        for (int i = 0; i < letterList.Count; i++)
        {
            GameObject clone;
            clone = Instantiate (prefabToSpawn, goToPutWordsIn.transform);
            clone.transform.parent = goToPutWordsIn.transform;
            clone.gameObject.GetComponent<DragHandler>().fosterParent = parentToFoster;

            GameObject otherclone;
            otherclone = Instantiate (emptyPrefabToSpawn, emptyWordsHere.transform);
            otherclone.transform.parent = emptyWordsHere.transform;
            
            TopTiles.Add(clone);
            BottomTiles.Add(otherclone);

            stringToSprite.TryGetValue(letterList[i].ToString(), out Sprite letter);
            clone.GetComponent<ImageAssign>().ChangeImage(letter,letterList[i]);
        }
         
         AssignAnswers();
    }
    

    public void AssignAnswers()
    {
        for (int i = 0; i < BottomTiles.Count; i++)
        {
            BottomTiles[i].gameObject.GetComponent<DropZone>().charmanderToWin = wordAsList[i];
        }
        
    }

    public void Reset()
    {
        foreach (var go in TopTiles)
        {
            Destroy(go);
        }

        TopTiles.Clear();
        
        foreach (var go in BottomTiles)
        {
            Destroy(go);
        }
        BottomTiles.Clear();
        
        wordAsList.Clear();
        letterList.Clear();
        
    }
    
}
