﻿using UnityEngine;
using System.Collections.Generic;
 
namespace UnityEngine.UI
{
    [AddComponentMenu("Layout/Auto Fill Grid Layout", 152)]
    public class AutoFillGridLayout : LayoutGroup
    {
        
        [SerializeField] protected int m_RowCount = 2;
        [SerializeField] protected int m_ColumnCount = 2;
        public int rowCount { get { return m_RowCount; } set { SetProperty(ref m_RowCount, Mathf.Max(1, value)); } }
        public int colCount{ get { return m_ColumnCount; } set { SetProperty(ref m_ColumnCount, Mathf.Max(1, value)); } }
        [SerializeField] protected Vector2 m_Spacing = Vector2.zero;
        public Vector2 spacing { get { return m_Spacing; } set { SetProperty(ref m_Spacing, value); } }

        private float widthOfObj;
        private float heightOfObj;

        private Vector2 cellSize;

        
        
        protected AutoFillGridLayout()
        {}

        public void GetWidthAndHeight (){
            widthOfObj = this.rectTransform.rect.size.x;
            heightOfObj = this.rectTransform.rect.size.y;
        }

        public override void SetLayoutHorizontal()
        {
           SetCellsAlongAxis(0);
        }

        public override void SetLayoutVertical()
        {
            SetCellsAlongAxis(1);
        }

        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();
            GetWidthAndHeight();
             cellSize.x = widthOfObj/colCount;

            SetLayoutInputForAxis(padding.horizontal + (cellSize.x), padding.horizontal + (cellSize.x),-1,0);
        }

        public override void CalculateLayoutInputVertical()
        {
            GetWidthAndHeight();
            cellSize.y = heightOfObj/rowCount;

            SetLayoutInputForAxis(padding.horizontal + (cellSize.y), padding.horizontal + (cellSize.y),-1,0);
            
        }

        private void SetCellsAlongAxis (int axis)
        {

            if(axis == 0){
                for (int i = 0; i < rectChildren.Count; i++)
                {
                    RectTransform rect = rectChildren[i];

                    m_Tracker.Add(this, rect,
                        DrivenTransformProperties.Anchors |
                        DrivenTransformProperties.AnchoredPosition |
                        DrivenTransformProperties.SizeDelta
                    );

                    rect.anchorMin = Vector2.up;
                    rect.anchorMax = Vector2.up;
                    rect.sizeDelta = new Vector2 (widthOfObj/colCount,heightOfObj/rowCount);
                }
                return;
            }


            //not sure from here

            int cellsPerMainAxis, actualCellCountX, actualCellCountY;
                cellsPerMainAxis = colCount;
                actualCellCountX = Mathf.Clamp(colCount, 1, rectChildren.Count);
                actualCellCountY = Mathf.Clamp(rowCount, 1, Mathf.CeilToInt(rectChildren.Count / (float)cellsPerMainAxis));
            


            Vector2 requiredSpace = new Vector2(
                actualCellCountX * cellSize.x + (actualCellCountX - 1) * spacing.x,
                actualCellCountY * cellSize.y + (actualCellCountY - 1) * spacing.y
            );
            Vector2 startOffset = new Vector2(
                GetStartOffset(0, requiredSpace.x),
                GetStartOffset(1, requiredSpace.y)
            );

            for (int i = 0; i < rectChildren.Count; i++)
            {
                int positionX;
                int positionY;
        
                    positionX = i % cellsPerMainAxis;
                    positionY = i / cellsPerMainAxis;
                
                    positionX = actualCellCountX - 1 - positionX;

                SetChildAlongAxis(rectChildren[i], 0, startOffset.x + (cellSize[0] + spacing[0]) * positionX, cellSize[0]);
                SetChildAlongAxis(rectChildren[i], 1, startOffset.y + (cellSize[0] + spacing[0]) * positionY, cellSize[0]);
            }

            
        }

    }



    



























}
