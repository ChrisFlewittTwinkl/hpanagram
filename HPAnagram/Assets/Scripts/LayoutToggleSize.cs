﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    using UnityEngine.UI;

    public class LayoutToggleSize : MonoBehaviour
    {
        public HorizontalLayoutGroup groupToToggle;
    // Start is called before the first frame update
    public void ToggleGroup(bool setTo)
    {
        groupToToggle.childControlHeight = setTo;
        groupToToggle.childControlWidth = setTo;
    }
}
