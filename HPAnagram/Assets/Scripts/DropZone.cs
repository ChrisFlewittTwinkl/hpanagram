﻿using System.Collections;
using System.Collections.Generic;
using TreeEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IDropHandler
{
    public GameObject imageOnMe;

    public bool correct;

    public char charmanderToWin;

   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDrop(PointerEventData eventData)
    {
        imageOnMe = eventData.pointerDrag;
        RectTransform imageRect = imageOnMe.GetComponent<RectTransform>();
        if (transform.childCount == 0)
        {
            Debug.Log("Shit its on me");
            
            imageOnMe.transform.SetParent(this.transform);
            imageRect.anchorMin = new Vector2(0.0f, 0.0f);
            imageRect.anchorMax = new Vector2(1.0f, 1.0f);

            imageRect.anchoredPosition = new Vector2(0.0f, 0.0f);
            imageRect.offsetMin = new Vector2(0.0f, 0.0f);
            imageRect.offsetMax = new Vector2(0.0f, 0.0f);
        }
        else
        {
            imageOnMe.transform.parent = imageOnMe.GetComponent<DragHandler>().startingParent;
            imageRect.offsetMin = new Vector2(0.0f, 0.0f);
            imageRect.offsetMax = new Vector2(0.0f, 0.0f);
            
        }

        Check();
    }

    public void Check()
    {
        if (imageOnMe != null)
        {
            if (transform.GetChild(0).gameObject.GetComponent<ImageAssign>().charToExpect == charmanderToWin && transform.childCount == 1)
            {
                correct = true;
            }
            else
            {
                correct = false;
            }
        }

        
    }
}
