using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/AnagramScriptable", order = 1)]
public class AnagramScriptable : ScriptableObject
{
    [System.Serializable]
    public class DataSets
    {
        public string wordAnagram;
        public Sprite hintImage;
    }

    public List<DataSets> setsOfData = new List<DataSets>();
}
