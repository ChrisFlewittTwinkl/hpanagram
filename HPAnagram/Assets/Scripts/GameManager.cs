﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class GameManager : MonoBehaviour
{
    public WordToTile wtTile;
    public List<GameObject> bTiles = new List<GameObject>();
    private bool wonState;
    public AnagramScriptable scriptableOption;
    public Image hintUIImage;
    public int score;
    
    public List<string> allStrings = new List<string>();
    public List<Sprite> allSprites = new List<Sprite>();
    private AudioSource winAudio;
    public UIManager uiMan;
    public Animator growAnimCont;
    public AnimationClip growAnim;

    
    private void Start()
    {
        winAudio = GetComponent<AudioSource>();
        wtTile.PopulateDictionary();
        PopulateLists();
        ChooseAnagram();
    }

    private void Update() {
        if(Input.GetButtonUp("Fire1")){
            TriggerCheck();
        }
    }

    private void PopulateLists()
    {
        for (int i = 0; i < scriptableOption.setsOfData.Count; i++)
        {
            allSprites.Add(scriptableOption.setsOfData[i].hintImage);
            allStrings.Add(scriptableOption.setsOfData[i].wordAnagram);
        }
    }

    public void ChooseAnagram()
    {
        int rand = Random.Range(0, allStrings.Count);
        wtTile.wordToPlayWith = allStrings[rand];
        hintUIImage.sprite = allSprites[rand];
        allSprites.Remove(allSprites[rand]);
        allStrings.Remove(allStrings[rand]);
        
        wtTile.PullLetter();
        
    }
    

    public void TriggerCheck()
    {
        wonState = Check();
        if (wonState)
        {
            winAudio.Play();
            StartCoroutine(GrowAndMove());
        }
        else
        {
            Debug.Log("No Win");
        }
        
    }

    private IEnumerator GrowAndMove( )
    {
            growAnimCont.SetTrigger("Grow");
            yield return new WaitForSeconds(growAnim.length);
            
            if (allStrings.Count > 0)
            {
                score++;
                wtTile.Reset();
                ChooseAnagram();
            }
            else
            {

                wtTile.Reset();
                Debug.Log("Smashed it");
                uiMan.EndScreen();

            }
        
    }


    public bool Check()
    {
        bTiles = wtTile.BottomTiles;

        for (int i = 0; i < bTiles.Count; i++)
        {
            bTiles[i].GetComponent<DropZone>().Check();
            if (!bTiles[i].GetComponent<DropZone>().correct)
            {
                return false;
            }
        }

        return true;
    }
    
}
