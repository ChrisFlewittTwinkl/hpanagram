﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ImageAssign : MonoBehaviour
{
    public Image imageToChange;

    public char charToExpect;
    
    // Start is called before the first frame update
    public void ChangeImage(Sprite toThis, char thisChar)
    {
        imageToChange.sprite = toThis;
        charToExpect = thisChar;
    }
}
